FROM ros:rolling

ENV ROS_DISTRO=rolling
ENV MOWITO_WS_2=/root/mowito_ws_2
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

# install ros packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ros-$ROS_DISTRO-desktop \
    && rm -rf /var/lib/apt/lists/*

# install other packages
RUN apt-get update && apt-get install -y --no-install-recommends  \
    build-essential software-properties-common \
    python3-pip python3-setuptools \
    python3-catkin-tools \
    python3-colcon-common-extensions \
    python3-vcstool \
    nano && \
    echo "alias python='python3'" >> /root/.bash_aliases && \
    echo "alias pip='pip3'" >> /root/.bash_aliases && \
    apt clean && rm -rf /var/lib/apt/lists/*

WORKDIR $MOWITO_WS_2

COPY ./ ./

# install dependencies
RUN apt update -qq \
    && rosdep update \
    && rosdep install --rosdistro $ROS_DISTRO --from-paths src --ignore-src -r -y \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y --no-install-recommends \
     ros-$ROS_DISTRO-moveit \
     ros-$ROS_DISTRO-moveit-common \
     ros-$ROS_DISTRO-controller-interface \
     ros-$ROS_DISTRO-joint-trajectory-controller \
     ros-rolling-warehouse-ros-mongo* \
     && rm -rf /var/lib/apt/lists/*

RUN echo "source /opt/ros/rolling/setup.bash" >> ~/.bashrc

CMD ["bash"]

