#!/bin/bash

XAUTH=/tmp/.docker.xauth

WORKSPACE_PATH="$HOME/mowito_ws_2"

echo "$WORKSPACE_PATH"

mkdir -p "$WORKSPACE_PATH/src"

cd "$WORKSPACE_PATH"

git clone https://github.com/UniversalRobots/Universal_Robots_ROS2_Driver.git src/Universal_Robots_ROS2_Driver
cd "$WORKSPACE_PATH"

vcs import src --skip-existing --input "src/Universal_Robots_ROS2_Driver/Universal_Robots_ROS2_Driver.repos"

sudo docker build -t arm_control .

if [ ! -f $XAUTH ]
then
    xauth_list=$(xauth nlist :0 | sed -e 's/^..../ffff/')
    if [ ! -z "$xauth_list" ]
    then
        echo $xauth_list | xauth -f $XAUTH nmerge -
    else
        touch $XAUTH
    fi
    chmod a+r $XAUTH
fi

sudo docker run -it \
    --env="DISPLAY=$DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --env="XAUTHORITY=$XAUTH" \
    --volume="$XAUTH:$XAUTH" \
    --runtime=nvidia \
    arm_control \
    bash