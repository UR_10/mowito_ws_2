#!/bin/bash

WORKSPACE_PATH="/root/mowito_ws_2"

colcon build --cmake-args -DCMAKE_BUILD_TYPE=Release

cd "$WORKSPACE_PATH/src"

git clone https://gitlab.com/mowito_arm/arm_control.git -b ros2

cd "$WORKSPACE_PATH"

source install/setup.bash

colcon build --cmake-args -DCMAKE_BUILD_TYPE=Release