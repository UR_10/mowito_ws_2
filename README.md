# mowito_ws_2



## Getting started

- Install the docker container from [here](https://docs.docker.com/engine/install/ubuntu/)
- `chmod u+x manipulation_setup.sh`
- `./manipulation_setup.sh`
- Running the above script will take you inside the docker container
- `chmod u+x manipulation_build.sh`
- `./manipulation_build.sh`

